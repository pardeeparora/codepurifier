<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LeftTreeItemDescription */

$this->title = 'Create Left Tree Item Description';
$this->params['breadcrumbs'][] = ['label' => 'Left Tree Item Descriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="left-tree-item-description-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
