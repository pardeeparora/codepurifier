<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\LeftTreeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Left Trees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="left-tree-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Left Tree', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'root',
            //'name',
            //'icon',
            //'icon_type',
            //'active',
            //'selected',
            //'disabled',
            //'readonly',
            //'visible',
            //'collapsed',
            //'movable_u',
            //'movable_d',
            //'movable_l',
            //'movable_r',
            //'removable',
            //'removable_all',
            //'child_allowed',

            'columns' => [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {add_description}',  // the default buttons + your custom button
                'buttons' => [
                    'add_description' => function($url, $model, $key) {     // render your custom button
                        return Html::a('<span class="glyphicon glyphicon-cog"></span>',\yii\helpers\Url::to(['left-tree-item-description/create','left_tree_id'=> $model->id]),['data-pjax'=>'0','title' => 'Description Update']);
                }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
