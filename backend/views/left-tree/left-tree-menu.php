<?php
/**
 * Created by PhpStorm.
 * User: Pardeep
 * Date: 8/16/18
 * Time: 4:52 PM
 */
use common\models\LeftTree;
use kartik\tree\TreeView;
echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => $query,
    'headingOptions' => ['label' => 'Categories'],
    'fontAwesome' => false,    // optional
    'isAdmin' => false,        // optional (toggle to enable admin mode)
    'displayValue' => 1,       // initial display value
    'softDelete' => true,      // defaults to true
    'cacheSettings' => [
        'enableCache' => true  // defaults to true
    ]
]);