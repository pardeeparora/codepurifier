<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "left_tree_item_description".
 *
 * @property int $id
 * @property string $content
 * @property int $left_tree_id
 *
 * @property LeftTree $leftTree
 */
class LeftTreeItemDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'left_tree_item_description';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['left_tree_id'], 'integer'],
            [['left_tree_id'], 'exist', 'skipOnError' => true, 'targetClass' => LeftTree::className(), 'targetAttribute' => ['left_tree_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'left_tree_id' => 'Left Tree ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeftTree()
    {
        return $this->hasOne(LeftTree::className(), ['id' => 'left_tree_id']);
    }
}
