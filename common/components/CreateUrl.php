<?php
/**
 * Created by PhpStorm.
 * User: Pardeep
 * Date: 8/22/2018
 * Time: 12:08
 */

namespace common\components;

use common\helpers\UtilityHelper;
use Yii;
use yii\base\Component;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class CreateUrl extends Component
{

    public function navigationButton($parent, $submenu)
    {
        $childSlug = UtilityHelper::createSlug("$submenu");
        $parentSlug = UtilityHelper::createSlug("$parent");
        $slug = $parentSlug.'/'.$childSlug;
        return $this->_generateCountrySpecificUrl($slug);
    }

    public function previousButton($parent, $submenu)
    {
        $childSlug = UtilityHelper::createSlug("$submenu");
        $parentSlug = UtilityHelper::createSlug("$parent");
        $slug = $parentSlug.'/'.$childSlug;
        return $this->_generateCountrySpecificUrl($slug);
    }

    public function _generateCountrySpecificUrl($slug)
    {
        $domain = UtilityHelper::getDomain();
        $protocol = 'http';
        $path = $slug;
        return "$protocol://$domain$path";
    }

    public function users($firstName, $lastName, $userid, $countryISOCode, $route = null)
    {
        $slug = UtilityHelper::createSlug("$firstName $lastName $userid");
        if ($route) {
            return $this->_generateCountrySpecificUrl($countryISOCode, $slug) . "/$route";
        }

        return $this->_generateCountrySpecificUrl($countryISOCode, $slug);
    }

    public function categoryCouponUrl($catSlug = null, $parents = null)
    {
        $parentPath = "/";
        if (is_array($parents)) {
            foreach ($parents as $parent) {
                $parentPath .= $parent['catSlug'] . "/";
            }
        }
        $parentPath = '/sale/categories' . ($parentPath != '//' ? $parentPath : '/');
        return Url::to([$parentPath . $catSlug], WEBSITE_PROTOCOL);
    }

    public function categoryUrl($catSlug, $parents = null)
    {
        $parentPath = "/";
        if (is_array($parents)) {
            foreach ($parents as $parent) {
                $parentPath .= $parent['catSlug'] . "/";
            }
        }
        return Url::to([$parentPath . $catSlug], WEBSITE_PROTOCOL);
    }

    public function collectionUrl($collId, $collslug = null, $countryISOCode = null, $isIdea = false)
    {
        $collslug = UtilityHelper::slugToText($collslug);
        $slug = UtilityHelper::createSlug("$collslug $collId");

        if (!$countryISOCode) {
            $countryISOCode = Yii::$app->country->countryISOCode;
        }

        if ($isIdea) {
            $slug = 'idea/' . $slug;
        } else {
            $slug = 'collection/' . $slug;
        }

        return $this->_generateCountrySpecificUrl($countryISOCode, $slug);
    }

    public function trendCollectionUrl($collslug = null, $countryISOCode = null, $isBrand = false)
    {
        $collslug = UtilityHelper::slugToText($collslug);
        $slug = UtilityHelper::createSlug("$collslug");

        if (!$countryISOCode) {
            $countryISOCode = Yii::$app->country->countryISOCode;
        }

        if ($isBrand == true) {
            $slug = 'trends/b/' . $slug;
        } else {
            $slug = 'trends/s/' . $slug;
        }

        return $this->_generateCountrySpecificUrl($countryISOCode, $slug);
    }

    /**
     * @param $url string -> The target url to which user is to be redirected
     * @param $type string ->  the type of entity which is being redirected
     * @param $l string -> in case of Direct Link it is : the link itself
     *
     *
     * @param $v string -> in case of direct link it is the : referral page
     *
     *+
     * @param null $c
     * @param null $a
     * @return string
     */
    public function redirectUrl($url, $type, $l, $v, $c = null, $a = null)
    {

        switch ($type) {
            case self::REDIRECT_TYPE_PRODUCT:
                if (!$c) {
                    $c = $this->_getRedirectSource('Product');
                }
                $a = "Click";
                break;
            case self::REDIRECT_TYPE_COUPAN :
                if (!$c) {
                    $c = $this->_getRedirectSource('Coupan');
                }
                $a = "Click";

                break;
            case self::REDIRECT_TYPE_SALE :
                if (!$c) {
                    $c = $this->_getRedirectSource('Sale');
                }
                $a = "Click";

                break;
            case self::REDIRECT_TYPE_DIRECT_LINK :
                if (!$c) {
                    $c = $this->_getRedirectSource('Direct');
                }
                if (!$v) {
                    $v = Url::to([''], WEBSITE_PROTOCOL);
                }
                $a = "Click";
                break;
        }

        return \yii\helpers\Url::to([
            '/redirect.html',
            'url' => urlencode($url),
            'type' => $type,
            'c' => $c,
            'a' => $a,
            'l' => $l,
            'v' => $v
        ], WEBSITE_PROTOCOL);
    }

    private function _getRedirectSource($type)
    {

        $route = Yii::$app->controller->route;

        $route = str_replace('/', '-', $route);

        return "R_" . $type . "_" . Yii::$app->country->countryISOCode . "_" . $route;
    }

    public function getCountrySpecificHomeUrl($countryISOCode)
    {
        $subDomain = UtilityHelper::isoCodeToSubdomain($countryISOCode);
        $domain = UtilityHelper::getDomain();
        $protocol = WEBSITE_PROTOCOL;
        if (empty($subDomain))
            return "$protocol://$domain";
        else
            return "$protocol://$subDomain.$domain";
    }

    public function brandUrl($brandSlug, $countryISOCode = null)
    {
        if (!$countryISOCode) {
            $countryISOCode = Yii::$app->country->countryISOCode;
        }

        $slug = 'brand/' . $brandSlug;

        return $this->_generateCountrySpecificUrl($countryISOCode, $slug);
    }

    public function dealsAndCoupanUrl($slug, $type, $storeName, $countryISOCode = null)
    {
        $url = $this->storeAffUrl($storeName, "",
                $countryISOCode) . '/' . COUPAN_PAGE_SLUG . UtilityHelper::createSlug($storeName);
        if (!empty($type)) {
            return $url . '?type=' . $type;
        } else {
            return $url;
        }
    }

    /**
     * @param $nameOrSlug
     * @param string $id
     * @param null $countryISOCode
     * @param bool $isStore tells if its a affilliate or store, since we are not showing stores any more, affiliate are the stores hence this flag is not required
     * @return string
     */
    public function storeAffUrl($nameOrSlug, $id = "", $countryISOCode = null, $isStore = false)
    {
        $slug = UtilityHelper::createSlug("$nameOrSlug");

        #sincee our affiliate is store, then there is nothing like affiliate
        /* if($isStore)
            $slug ="store/$slug";
        else  */

        $slug = "store/$slug";

        if (!$countryISOCode) {
            $countryISOCode = Yii::$app->country->countryISOCode;
        }
        return $this->_generateCountrySpecificUrl($countryISOCode, $slug);
    }

    public function homePageHashTags($selectedCat, $listType)
    {
        $id = '#cat_' . $selectedCat;
        return "?cat_id=" . $selectedCat . '&listType=' . $listType . $id;
    }

    public function categoryFilterUrl($catSlug, $parent = null, $grantParent = null)
    {
        $parentPath = "/";
        if (is_array($parent)) {
            foreach ($parent as $parentSlug) {
                $parentPath .= $parentSlug['catSlug'] . "/";
            }
        }
        $grantPath = '/';
        if (is_array($grantParent)) {
            foreach ($grantParent as $grantSlug) {
                $grantPath .= $grantSlug['catSlug'] . "";
            }
        }

        return Url::to([$grantPath . $parentPath . $catSlug], WEBSITE_PROTOCOL);

    }
}