<?php

namespace common\helpers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Description of UtilityHelper
 *
 * @author Asaan
 */
class UtilityHelper
{

    /**
     * Checks Cache Control header to tell if page was refreshed or not
     *
     * if request is ajax, checks the last normal request
     * @return bool|void
     */
    public static function checkIsRefresh()
    {
        $cond1 = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
        $cond2 = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'no-cache';

        $result = $cond1 || $cond2;

        Yii::$app->session->set('last-req-referesh', $result);

        if (Yii::$app->request->isAjax) {
            return Yii::$app->session->get('last-req-referesh');
        }

        return $result;
    }

    /**
     * This function returns current subdomain or a blank string "" in case of US
     * @return string
     */
    public static function getSubdomain()
    {
        $subDomain = "";
        $host = Yii::$app->request->hostName;
        $host = explode(".", $host);
        count($host) > 2 && $subDomain = @$host[0];
        return $subDomain;
    }

    /**
     * Returns current domain
     * @return string
     */
    public static function getDomain()
    {
        $host = Yii::$app->request->hostName;
        $homeUrl = Yii::$app->homeUrl;
        return $host.$homeUrl;
    }

    /**
     *
     * @param $subDomain
     * @return mixed
     */
    public static function subdomainToISOCode($subDomain)
    {
        $countryISOCode = null;
        $subDomain = strtolower($subDomain);

        $countryDetails = array_filter(Yii::$app->country->getCountries(), function ($country) use ($subDomain) {
            return $country['subDomain'] == $subDomain;
        });

        if (empty($countryDetails)) {
            throw new InvalidParamException("Invalid Country ISO Code passed");
        }

        return array_pop($countryDetails)['countryISOCode'];
    }

    public static function isoCodeToSubdomain($countryISOCode)
    {

        if (!$countryISOCode) {
            $countryISOCode = Yii::$app->country->countryISOCode;
        }
        // todo
        // below condition remove later
        if ($countryISOCode == 'KSA') {
            $countryISOCode = 'SA';
        }

        if ($countryISOCode == 'SAU') {
            $countryISOCode = 'SA';
        }

        if ($countryISOCode == 'UAE') {
            $countryISOCode = 'AE';
        }

        $countryISOCode = strtoupper($countryISOCode);
        $countryDetails = array_filter(Yii::$app->country->getCountries(true),
            function ($country) use ($countryISOCode) {
                return $country['countryISOCode'] === $countryISOCode;
            });

        if (empty($countryDetails)) {
            throw new InvalidParamException("Invalid Country ISo Code passed");
        }

        return array_pop($countryDetails)['subDomain'];

    }

    /**
     * recursivly check if an array is empty i.e it has empty key value pairs
     * @param $value
     * @return bool
     */
    public static function checkEmptyArrayRecursive($value)
    {
        if (is_array($value)) {
            $empty = true;
            array_walk_recursive($value, function ($item) use (&$empty) {
                $empty = $empty && empty($item);
            });
        } else {
            $empty = empty($value);
        }
        return $empty;
    }

    public static function createSlug($text)
    {

        // if its is already a slug, do nothing and return
        if (preg_match('/^[a-z][-a-z0-9]*$/', $text)) {
            return $text;
        }

        $text = str_replace('-', '', $text);
        // replace non letter or digits by -
        //$text = preg_replace('~[^\pL\d]+~u', '-', $text);


        $text = preg_replace("/[^A-Za-z0-9 ]/", '', $text);
        $text = preg_replace('/ +/', '-', $text);

        // transliterate
        //$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        // $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        // $text = trim($text, '-');

        // remove duplicate -
        // $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public static function slugToText($slug)
    {
        $rawText = explode('-', $slug);
        $text = implode(' ', $rawText);
        $text = strtoupper($text);
        return $text;
    }


    public static function refineResult($rawProductlist)
    {
        $finalProductList = [];
        foreach ($rawProductlist as $product) {
            $temp = $product;
            $temp['discount'] = self::calculateDiscount(@$temp['price'], @$temp['dscntdPrice']);
            $temp['likeCount'] = self::getLikeCount(@$temp['likeCount']);
            $temp['profileImage'] = self::getProfileImage(@$temp['curator']['firstName'],
                @$temp['curator']['profileImage']);
            $temp['proLtLk'] = self::getProductLikeProfileImage(@$temp['usrLkLst']);
            $finalProductList[] = $temp;
        }
        return $finalProductList;
    }

    public static function calculateDiscount($price, $discount_price)
    {
        if (!empty($price) && !empty($discount_price)) {
            $discount = (($price - $discount_price) / $price) * 100;
            return round($discount, 0);
        }
    }

    public static function getLikeCount($likeCount, $defaultLikeCount = self::DEFAULT_LIST_LIKE)
    {
        if ($likeCount <= $defaultLikeCount) {
            return null;
        } else {
            return '+' . ($likeCount - $defaultLikeCount);
        }
    }

    /**
     * @param $firstName
     * @param $profileImage
     * @param bool $socialImagesAllowed Defines whether to return user images for social sites are not. T
     * required for sitemap since, foreign images are not allowed in sitemap
     * @return null|string
     */
    public static function getProfileImage($firstName, $profileImage, $socialImagesAllowed = true)
    {
        /*sometimes the $profileImage response is null string*/
        if (!empty($profileImage) && $profileImage != 'null' && $profileImage != null) {
            /**
             * if social images are not allowed, then only return images hosted on asaan.
             */
            if ($socialImagesAllowed || strpos($profileImage, "asaan.com") !== false) {
                return $profileImage;
            }
        }


        $firstName = strtolower($firstName);
        $base_url = Yii::$app->urlManager->createAbsoluteUrl('images/dummy_image/', WEBSITE_PROTOCOL);
        $firstChar = @$firstName[0];
        $alphabet_array = [
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z'
        ];
        if (in_array($firstChar, $alphabet_array)) {
            $firstChar = strtolower($firstChar);
            return $base_url . '/' . $firstChar . '.png';
        } else {
            return $base_url . '/avatar.png';
        }
    }

    public static function getProductLikeProfileImage($productLtLkArray)
    {
        if (!empty($productLtLkArray)) {
            $likeProductProfileImage = [];
            foreach ($productLtLkArray as $proLtLk) {
                $tempData = $proLtLk;
                $tempData['profileImage'] = self::getProfileImage(@$proLtLk['firstName'], @$tempData['profileImage']);
                $likeProductProfileImage[] = $tempData;
            }
            return $likeProductProfileImage;
        }
    }

    public static function getTagArray($tagString)
    {
        return array_filter(explode('#', $tagString));
    }

    public static function getSlideImages($imagesArray){
        if(!is_array($imagesArray))
            return [];

        $imagesArray = array_filter($imagesArray['xxhdpi']);
        unset($imagesArray[0]);
        unset($imagesArray[1]);
        return $imagesArray;
    }

    public static function getUserId($slug)
    {
        if (empty($slug)) {
            return false;
        }
        return self::matchPattern($slug, PATTERN_USER);
    }

    public static function matchPattern($string, $pattern)
    {
        preg_match("/$pattern/", $string, $matches);
        if (@$matches[1]) {
            return strtoupper($matches[1]);
        }
        return false;
    }

    public static function getProductId($slug)
    {
        if (empty($slug)) {
            return false;
        }
        return self::matchPattern($slug, PATTERN_PRODUCT);
    }

    public static function userProfileDropdown()
    {
        $userModel = Yii::$app->user->identity;
        $fName = $userModel->firstName;
        $lName = $userModel->lastName;
        $userId = $userModel->userId;
        $countryISOCode = $userModel->countryISOCode;
        $logout = Yii::$app->urlManager->createAbsoluteUrl(['user/logout']);

        $userProfileDrop = [
            [
                'url' => Yii::$app->createUrl->users($fName, $lName, $userId, $countryISOCode, 'likes'),
                'imgUrl' => '/images/profile_xhdpi.png',
                'label' => 'My Profile',
                'isVisibleMobile' => false,
                'isVisibleDesktop' => true
            ],
            [
                'url' => Yii::$app->createUrl->users($fName, $lName, $userId, $countryISOCode, 'likes'),
                'imgUrl' => '/images/like_xhdpi.png',
                'label' => 'Likes',
                'isVisibleMobile' => true,
                'isVisibleDesktop' => true,
                'count' => count(@$userModel['prdctAppLkLst'])
            ],
            [
                'url' => Yii::$app->createUrl->users($fName, $lName, $userId, $countryISOCode, 'honks'),
                'imgUrl' => '/images/post_xhdpi.png',
                'label' => 'Honks',
                'isVisibleMobile' => true,
                'isVisibleDesktop' => true,
                'count' => @$userModel['postProductCount']
            ],
            [
                'url' => Yii::$app->createUrl->users($fName, $lName, $userId, $countryISOCode, 'followers'),
                'imgUrl' => '/images/follower_xhdpi.png',
                'label' => 'Followers',
                'isVisibleMobile' => true,
                'isVisibleDesktop' => true,
                'count' => @$userModel['followersCount']
            ],
            [
                'url' => Yii::$app->createUrl->users($fName, $lName, $userId, $countryISOCode, 'followings'),
                'imgUrl' => '/images/following_xhdpi.png',
                'label' => 'Followings',
                'isVisibleMobile' => true,
                'isVisibleDesktop' => true,
                'count' => @$userModel['followingsCount']
            ],
//            [
//                'url' => Url::to(['earning/overview']),
//                'imgUrl' => '/images/wallet_xxxhdpi.png',
//                'label' => 'Wallet',
//                'isVisibleMobile'=> true,
//                'isVisibleDesktop'=> true
//            ],
            [
                'url' => $logout,
                'imgUrl' => '/images/logout.png',
                'label' => 'Logout',
                'isVisibleMobile' => false,
                'isVisibleDesktop' => true
            ],

        ];

        return $userProfileDrop;
    }

    public static function encrypt($value)
    {
        return base64_encode(openssl_encrypt($value, 'AES-128-CBC', CRYPTO_SECURE_KEY, OPENSSL_RAW_DATA,
            substr(CRYPTO_SECURE_KEY, 0, 16)));
    }

    public static function decrypt($value)
    {
        return openssl_decrypt(base64_decode($value), 'AES-128-CBC', CRYPTO_SECURE_KEY, OPENSSL_RAW_DATA,
            substr(CRYPTO_SECURE_KEY, 0, 16));
    }

    /**
     * This function returns all constants
     * used in this class with specified prefix
     *
     * @param type $token
     * @param type $objectClass
     * @return type
     */
    public static function getConstants($token, $objectClass)
    {
        $tokenLen = strlen($token);

        $reflection = new \ReflectionClass($objectClass); //php built-in
        $allConstants = $reflection->getConstants(); //constants as array

        $tokenConstants = array();
        foreach ($allConstants as $name => $val) {
            if (substr($name, 0, $tokenLen) != $token) {
                continue;
            }
            $tokenConstants[$val] = self::constToString($name);
        }
        return $tokenConstants;
    }

    /**
     * EX> Inputs  =  TYPE_SELECTED
     *
     * @param $value
     */
    public static function constToString($value)
    {
        $out = explode("_", $value);
        #remove first element
        if (isset($out[0])) {
            unset($out[0]);
        }
        $out = implode(" ", $out);

        return BaseInflector::camel2words($out);
    }

    public static function shuffle_assoc($my_array)
    {
        $keys = array_keys($my_array);

        shuffle($keys);

        foreach ($keys as $key) {
            $new[$key] = $my_array[$key];
        }

        $my_array = $new;

        return $my_array;
    }

    public static function time_elapsed_string($datetime, $full = false)
    {
        $now = new \DateTime();
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        if (!$full) {
            $string = array_slice($string, 0, 1);
        }
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }


    public static function GetImageDetails($url)
    {
        if (empty($url)) {
            return false;
        }

        $response = new Response();
        $imageStatus = false;
        $fileName = pathinfo($url, PATHINFO_FILENAME);
        if (preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $fileName)) {
            $string = substr($fileName, strrpos($fileName, '_') + 1);
            $heightWidth = explode('x', $string);
            $width = @$heightWidth[0];
            $height = @$heightWidth[1];
            if (is_numeric($width) && is_numeric($height)) {
                $imageStatus = true;
                return $response->data = [
                    'imageUrl' => $url,
                    'width' => $width,
                    'height' => $height
                ];
            }
        }

        if ($imageStatus == false) {
            $heightWidth = getimagesize($url);
            $width = @$heightWidth[0];
            $height = @$heightWidth[1];
            return $response->data = [
                'imageUrl' => $url,
                'width' => $width,
                'height' => $height
            ];
        }
    }

    public static function renderCategoryFilterTree($data)
    {

        $renderedTree = "";

        foreach ($data as $node) {
            $renderedTree .= self::printTree($node);
        }

        return $renderedTree;
    }

    private static function printTree($node)
    {
        $renderedTree = "";

        if (!isset($node['children']) || count($node['children']) == 0) {
            $renderedTree .= self::render($node, true, false, true);
        } else {
            //if not a leaf node
            $renderedTree .= self::render($node, true);

            if (count($node['children'])) {
                foreach ($node['children'] as $innerNode) {
                    $renderedTree .= self::printTree($innerNode);
                }
            }
            $renderedTree .= self::render($node, true, true);
        }

        return $renderedTree;
    }

    private static function render($node, $isParent = false, $isParentEnd = false, $isLeaf = false)
    {
        $template = "";

        if ($isParentEnd) {
            $template = "</ul></li>";
            return $template;
        }


        //extra class for mobile
        $mobileClass = Yii::$app->device->type == Device::DEVICE_MOBILE ? "_mobile" : "";

        if ($isLeaf && $isParent && !$isParentEnd) {
            $template = '<li class="list-group-item" >
            <a class="toggle" href="javascript:void(0);"><input id="{{label}}' . $mobileClass . '" {{selectedAttr}} class="select-filter" name="{{name}}" value="{{value}}" type="{{type}}"> <label for="{{label}}' . $mobileClass . '" > <span></span>{{label}} </label>
            </a>             
    </li>';
        } else {
            if ($isParent && !$isParentEnd && !$isLeaf) {
                $template = '<li class="list-group-item">
        <a class="toggle" href="javascript:void(0);"><input id="{{label}}' . $mobileClass . '" {{selectedAttr}} class="select-filter" name="{{name}}" value="{{value}}" type="{{type}}"> <label for="{{label}}' . $mobileClass . '" > <span></span>{{label}} </label>    
    </a>
        <ul  class="inner {{selectedClass}} list-unstyled">';
            }
        }

        $template = str_replace([
            '{{type}}',
            '{{label}}',
            '{{name}}',
            '{{value}}',
            '{{selectedClass}}',
            '{{selectedAttr}}',
        ], [

//            count($node['children']) ? "radio":"checkbox",
            $node['type'],
            $node['label'],
            $node['name'],
            $node['catId'],
            $node['checked'] ? "show" : "",
            $node['checked'] ? "checked" : ""
        ], $template);

        return $template;
    }

    public static function generateCategoryFilterView($categories, $maxDepth)
    {
        $finalArray = [];
        $categories = array_values($categories);
        $newCategory = [];
        for ($j = 0; $j < count($categories); $j++) {
            for ($i = 0; $i <= count($categories[$j]['parents']); $i++) {
                if ($i == 0) {
                    $newCategory[$j] = self::getCatNode($categories[$j], $i, $maxDepth);
                } else {
                    if ($i == 1) {
                        $newCategory[$j]['children'] = self::getCatNode($categories[$j], $i, $maxDepth);
                    } else {
                        if ($i == 2) {
                            $newCategory[$j]['children']['children'] = self::getCatNode($categories[$j], $i, $maxDepth);
                        } else {
                            if ($i == 3) {
                                $newCategory[$j]['children']['children']['children'] = self::getCatNode($categories[$j],
                                    $i, $maxDepth);
                            } else {
                                if ($i == 4) {
                                    $newCategory[$j]['children']['children']['children']['children'] = self::getCatNode($categories[$j],
                                        $i, $maxDepth);
                                } else {
                                    if ($i == 5) {
                                        $newCategory[$j]['children']['children']['children']['children']['children'] = self::getCatNode($categories[$j],
                                            $i, $maxDepth);
                                    } else {
                                        if ($i == 6) {
                                            $newCategory[$j]['children']['children']['children']['children']['children']['children'] = self::getCatNode($categories[$j],
                                                $i, $maxDepth);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $finalTree = [];
        foreach ($newCategory as $node) {
            self::insertNode($finalTree, $node);
        }

        return $finalTree;
    }

    private static function getCatNode($categories, $i, $maxDepth)
    {
        $cat = [];
        if ($i == count($categories['parents'])) {
            unset($categories['parents']);
            $cat = $categories;
            $cat['children'] = [];
        } else {
            $cat = $categories['parents'][$i];
        }
        unset($cat['parentIds']);

        $cat['type'] = $i == $maxDepth ? "checkbox" : "radio";

        if ($cat['type'] == "checkbox") {
            $cat['name'] = "category_" . $i . "[]";
        } else {
            $cat['name'] = "category_" . $i;
        }

        $inUrlValues = Yii::$app->request->get("category_" . $i);

        if (is_array($inUrlValues)) {
            $cat['checked'] = in_array($cat['catId'], $inUrlValues);
        } else {
            $cat['checked'] = $inUrlValues == $cat['catId'];
        }

        $cat['value'] = $cat['catId'];
        $cat['label'] = $cat['catName'];

        return $cat;
    }

    public static function insertNode(&$tree, $node)
    {

        $tempChild = [];
        if (!isset($node['children']) || empty($node['children'])) {
            $node['children'] = [];
        } else {
            $tempChild = $node['children'];
        }

        if (!isset($tree[$node['catId']])) {
            $node['children'] = [];
            $tree[$node['catId']] = $node;
        }

        if (!empty($tempChild)) {
            self::insertNode($tree[$node['catId']]['children'], $tempChild);
        }
        return $tree;
    }

    public static function getCategoryFilterData($requestData)
    {

        foreach ($requestData as $key => $value) {
            if (strpos($key, 'category') !== false && is_array($value)) {
                return $value;
            }
        }
        $catLevel = false;
        foreach ($requestData as $key => $value) {
            if (strpos($key, 'category') !== false) {
                $thisCatLevel = explode("_", $key)[1];
                $catLevel = max($thisCatLevel, $catLevel);
            }
        }
        if ($catLevel !== false) {
            return \app\models\Category::getChilds($requestData['category_' . $catLevel]);

        }

        return [];
    }

    public static function arrayStringEncode($array)
    {
        if (empty($array)) {
            return $array;
        }

        array_walk_recursive($array, function (&$item, $key) {
            $item = utf8_encode($item);
            $item = Html::encode($item);
        });

        return $array;
    }

    public static function normalizeURL($url)
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }


}
