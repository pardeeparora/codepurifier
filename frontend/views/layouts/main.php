<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\assets\AppAsset;
use common\components\Constant;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<span><i style="font-size: x-large" class="glyphicon glyphicon-filter"></i>codePurifier</span>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top custom-header',
        ],
    ]);

    $menuItems = [];
    $slug = Yii::$app->request->get('slug');
    $menuItems[] = ['label' => 'Html', 'url' => ['/html'],'active' => $slug == 'html'];
    $menuItems[] = ['label' => 'Css3', 'url' => ['/css3'],'active' => $slug == 'css3'];
    $menuItems[] = ['label' => 'Javascript', 'url' => ['/javascript'],'active' => $slug == 'javascript'];
    $menuItems[] = ['label' => 'jQuery', 'url' => ['/jquery'],'active' => $slug == 'jquery'];
    $menuItems[] = ['label' => 'Bootstrap', 'url' => ['/bootstrap'],'active' => $slug == 'bootstrap'];
    $menuItems[] = ['label' => 'Php', 'url' => ['/php'],'active' => $slug == 'php'];
    $menuItems[] = ['label' => 'Mysql', 'url' => ['/mysql'],'active' => $slug == 'mysql'];
    $menuItems[] = ['label' => 'Php Framework', 'url' => ['/php-framework'],'active' => $slug == 'php-framework'];


    if (Yii::$app->user->isGuest) {
        //$menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
      //  $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
</div>

<div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-left">
        <?php
        $actionId = Yii::$app->controller->action->id;
        $controller_id = Yii::$app->controller->id;
        $slug = Yii::$app->request->get('slug');
        $page = Yii::$app->request->get('page');
        $menuListArray = [];
        $homePage = false;
        if($actionId == Constant::HOME_ACTION && $controller_id == Constant::SITE_CONTROLLER){
            $homePage= true;

            $model = \common\models\LeftTree::find()->select('name,id')->where(['lvl' => 0])->asArray()->all();

            foreach ($model as $mainMenuList){

                $submenuList = \common\models\LeftTree::find()->select('id,name,root')->where(['lvl' => 1])->andWhere(['root' => $mainMenuList['id']])->asArray()->all();

                foreach ($submenuList as $list){
                    $menuListArray[$mainMenuList['name']][] = [
                        'name' => $list['name'],
                    ];
                }
            }

        } else {
            $name = strtoupper(str_replace('-',' ',$slug));
            $subQuery = \common\models\LeftTree::find()->select('root')->where(['name' => $name]);

            $menuListArray = \common\models\LeftTree::find()
                ->select(['name'])
                ->where(['in', 'root', $subQuery])
                ->andWhere(['lvl'=>2])
                ->asArray()->all();
        }
        ?>

        <?php if(!empty($menuListArray)): ?>
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
            <nav>
                <?php if($homePage): ?>
                <ul class="nav side-menu-list">
                        <?php foreach ($menuListArray as $key => $list): ?>
                    <li class="submenu-list"><b><?php echo $key ?></b>
                        <ul class="nav">
                            <?php foreach ($list as $innerList): ?>
                                <?php
                                $activeClass = '';
                                if(\common\helpers\UtilityHelper::createSlug($innerList['name']) == $page){
                                    $activeClass = 'class=active';
                                }
                                if ($homePage){
                                    $slug = \common\helpers\UtilityHelper::createSlug($key);
                                    $url = \yii\helpers\Url::to([$slug.'/'.\common\helpers\UtilityHelper::createSlug($innerList['name'])]);
                                } else{
                                    $url = \yii\helpers\Url::to([$slug.'/'.\common\helpers\UtilityHelper::createSlug($innerList['name'])]);
                            }
                                ?>
                            <li <?php echo $activeClass ?>><a href="<?php echo $url  ?>"><?php  echo $innerList['name'] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endforeach; ?>
                </ul>
                <?php else: ?>
                    <ul class="nav side-menu-list">
                        <?php foreach ($menuListArray as $key => $list): ?>
                            <?php
                            $activeClass = '';
                            $url = \yii\helpers\Url::to([$slug.'/'.\common\helpers\UtilityHelper::createSlug($list['name'])]);
                            if(\common\helpers\UtilityHelper::createSlug($list['name']) == $page){
                                $activeClass = 'class=active';
                            }
                            ?>
                            <li <?php echo $activeClass ?>>
                                <a href="<?php echo $url  ?>">
                                    <?php  echo $list['name'] ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

            </nav>
        </div><!--/span-->
        <?php endif; ?>

        <div class="col-xs-12 col-sm-9 content">
                <button style="display: none" type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Hide Menu</button>
            <?php echo $content ?>
        </div><!--/span-->

    </div><!--/row-->

</div>

<footer class="footer" style="margin-top: 100%">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script type='text/javascript'>
    $(document).ready(function () {
        $('[data-toggle=offcanvas]').click(function () {
            if ($('.sidebar-offcanvas').css('background-color') == 'rgb(255, 255, 255)') {
                $('.list-group-item').attr('tabindex', '-1');
            } else {
                $('.list-group-item').attr('tabindex', '');
            }
            $('.row-offcanvas').toggleClass('active');

        });
    });
</script>
