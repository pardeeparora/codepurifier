<div class="site-home">
    <div class="row">
        <div class="col-xs-12">
            <h2 style="font-weight: 900">Welcome to Code Purifier</h2>
            <p class="home-page-description jambotron">
                This website provides basic knowledge web and database programming. You can try your code in code editor which is provided by us.
                In this website we are providing thousands of example to improve the programming. You can use any one example and run the code editor there you will also see the outpout of the progam.
            </p>
            <br/><br/>
        </div>
        <div class="col-xs-12">
            <h2 style="font-weight: 900">Learn and Explore Yourself</h2>
            <p class="home-page-description jambotron">
                In this website we are providing thousands of example to improve the programming. You can use any one example and run the code editor there you will also see the outpout of the progam.
                This website provides basic knowledge web and database programming. You can try your code in code editor which is provided by us.
            </p>
            <p class="home-page-description">
                Grow yourself as a programmer. Learn allot and explore your skills to the world.
            </p>
            <br/><br/>
        </div>

        <div class="col-lg-12 technology-block">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-body text-center">
                    <span class="home-page-icons">
                        <i class="fa fa-mobile-phone"></i>
                    </span>
                        <h3>Responsive</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-body text-center">
                    <span class="home-page-icons">
                        <i class="fa fa-html5"></i>
                    </span>
                        <h3>HTML5</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-body text-center">
                    <span class="home-page-icons">
                        <i class="fa fa-database"></i>
                    </span>
                        <h3>MySql</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-body text-center">
                    <span class="home-page-icons">
                        <i class="fa fa-css3"></i>
                    </span>
                        <h3>CSS3</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">

            <div class="col-lg-8 padding-10">
                <div class="panel panel-default">
                    <div class="panel-heading">HTML Editor</div>
                    <div class="panel-body">
                        <pre><code>&lt;html&gt;&lt;head&gt;
                                &lt;p&gt;Sample text here...&lt;/p&gt;
&lt;p&gt;And another line of sample text here...&lt;/p&gt;
</code></pre>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <h1>HTML</h1>
            </div>

        </div>
    </div>
</div>
