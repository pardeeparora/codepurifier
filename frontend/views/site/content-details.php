<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <div class="col-xs-12">
            <?php
            if(isset($model) && !empty($model))
                echo $model->content;
            else
                echo '<div class="content-area">Sorry! No content found</div>'
            ?>
        </div>
        <div class="col-md-12">
            <div class="col-xs-6">
                <?php
                if (isset($buttons['previous'])) {
                    $previousButton = Yii::$app->createUrl->navigationButton(Yii::$app->request->get('slug'), $buttons['previous']);
                    echo \yii\helpers\Html::a('Previous', \yii\helpers\Url::to($previousButton), ['class' => 'btn btn-primary']);
                } else{
                    echo \yii\helpers\Html::a('Previous', '#', ['class' => 'btn btn-primary','disabled' => true]);
                }
                ?>
            </div>
            <div class="col-xs-6">
                <?php
                if (isset($buttons['next'])) {
                    $nextButton = Yii::$app->createUrl->navigationButton(Yii::$app->request->get('slug'), $buttons['next']);
                    echo \yii\helpers\Html::a('Next', \yii\helpers\Url::to($nextButton), ['class' => 'btn btn-primary']);
                } else{
                    echo \yii\helpers\Html::a('Next', '#', ['class' => 'btn btn-primary','disabled' => true]);
                }
                ?>
            </div>
        </div>
    </div>
</div>
