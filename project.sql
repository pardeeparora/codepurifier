-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.43-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table yii2advanced.left_tree
DROP TABLE IF EXISTS `left_tree`;
CREATE TABLE IF NOT EXISTS `left_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique tree node identifier',
  `root` int(11) DEFAULT NULL COMMENT 'Tree root identifier',
  `lft` int(11) NOT NULL COMMENT 'Nested set left property',
  `rgt` int(11) NOT NULL COMMENT 'Nested set right property',
  `lvl` smallint(5) NOT NULL COMMENT 'Nested set level / depth',
  `name` varchar(60) NOT NULL COMMENT 'The tree node name / label',
  `icon` varchar(255) DEFAULT NULL COMMENT 'The icon to use for the node',
  `icon_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Icon Type: 1 = CSS Class, 2 = Raw Markup',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is active (will be set to false on deletion)',
  `selected` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is selected/checked by default',
  `disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is enabled',
  `readonly` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is read only (unlike disabled - will allow toolbar actions)',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is visible',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is collapsed by default',
  `movable_u` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is movable one position up',
  `movable_d` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is movable one position down',
  `movable_l` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is movable to the left (from sibling to parent)',
  `movable_r` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is movable to the right (from sibling to child)',
  `removable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is removable (any children below will be moved as siblings before deletion)',
  `removable_all` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is removable along with descendants',
  `child_allowed` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether to allow adding children to the node',
  PRIMARY KEY (`id`),
  KEY `left_tree_NK1` (`root`),
  KEY `left_tree_NK2` (`lft`),
  KEY `left_tree_NK3` (`rgt`),
  KEY `left_tree_NK4` (`lvl`),
  KEY `left_tree_NK5` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

-- Dumping data for table yii2advanced.left_tree: ~93 rows (approximately)
DELETE FROM `left_tree`;
/*!40000 ALTER TABLE `left_tree` DISABLE KEYS */;
INSERT INTO `left_tree` (`id`, `root`, `lft`, `rgt`, `lvl`, `name`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`, `child_allowed`) VALUES
	(1, 1, 1, 74, 0, 'PHP TUTORIALS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(2, 1, 16, 73, 1, 'PHP', '', 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(3, 1, 17, 18, 2, 'PHP DATABASE', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(4, 1, 2, 3, 1, 'PHP DB MANAGEMENT', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(5, 1, 4, 5, 1, 'GET POST METHOD', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(6, 1, 6, 7, 1, 'PHP ARRAY FUNCTIONS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(7, 1, 8, 9, 1, 'PHP STRINGS FUNCTIONS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(8, 1, 10, 11, 1, 'PHP SESSIONS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(9, 1, 12, 13, 1, 'PHP FILE SYSTEM', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(10, 1, 14, 15, 1, 'PHP OOPS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(11, 1, 19, 20, 2, 'PHP FILE INCLUDE', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(12, 1, 21, 22, 2, 'PHP OPEN FILE', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(13, 1, 23, 24, 2, 'PHP WRITE FILE', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(14, 1, 25, 26, 2, 'PHP READ FILE', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(15, 15, 1, 72, 0, 'HTML TUTORIALS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(16, 15, 2, 3, 1, 'HTML BASIC TAGS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(17, 15, 4, 5, 1, 'META TAGS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(18, 15, 6, 7, 1, 'HTML 5', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(19, 15, 8, 9, 1, 'SVG', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(20, 15, 10, 11, 1, 'HTML DOM', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(21, 15, 12, 71, 1, 'HTML', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(22, 15, 13, 14, 2, 'HTML INTRODUCTION', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(23, 15, 15, 16, 2, 'HTML ELEMENTS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(24, 15, 17, 18, 2, 'HTML IMAGES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(25, 15, 19, 20, 2, 'HTML ANCHOR', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(26, 15, 21, 22, 2, 'HTML BUTTONS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(27, 1, 27, 28, 2, 'PHP VERSION', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(28, 1, 29, 30, 2, 'PHP SYNTAX', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(29, 1, 31, 32, 2, 'PHP VARIABLES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(30, 1, 33, 34, 2, 'PHP ECHO', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(31, 1, 35, 36, 2, 'PHP DATA TYPES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(32, 1, 37, 38, 2, 'PHP CONSTANT', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(33, 1, 39, 40, 2, 'PHP STRINGS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(34, 1, 41, 42, 2, 'PHP CONDITIONAL', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(35, 1, 43, 44, 2, 'PHP WHILE LOOP', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(36, 1, 45, 46, 2, 'PHP FOREACH LOOP', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(37, 1, 47, 48, 2, 'PHP FOR LOOP', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(38, 1, 49, 50, 2, 'PHP SWITCH', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(39, 1, 51, 52, 2, 'PHP SORTING', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(40, 1, 53, 54, 2, 'PHP ARRAY', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(41, 1, 55, 56, 2, 'PHP FORM HANDLING', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(42, 1, 57, 58, 2, 'PHP FORM VALIDATION', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(43, 1, 59, 60, 2, 'PHP URI', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(44, 1, 61, 62, 2, 'PHP SESSIONS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(45, 1, 63, 64, 2, 'PHP COOKIES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(46, 1, 65, 66, 2, 'PHP FILTERS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(47, 1, 67, 68, 2, 'PHP DATE TIME', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(48, 1, 69, 70, 2, 'PHP TRY CATCH', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(49, 1, 71, 72, 2, 'PHP FILE UPLOAD', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(50, 15, 23, 24, 2, 'HTML ATTRIBUTES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(51, 15, 25, 26, 2, 'HTML HEADINGS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(52, 15, 27, 28, 2, 'HTML PARAGRAPHS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(53, 15, 29, 30, 2, 'HTML FORMATTING', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(54, 15, 31, 32, 2, 'HTML STYLES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(55, 15, 33, 34, 2, 'HTML QUOTATIONS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(56, 15, 35, 36, 2, 'HTML COLORS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(57, 15, 37, 38, 2, 'HTML COMMENTS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(58, 15, 39, 40, 2, 'HTML CSS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(59, 15, 41, 42, 2, 'HTML TABLES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(60, 15, 43, 44, 2, 'HTML LISTS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(61, 15, 45, 46, 2, 'HTML BLOCKS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(62, 15, 47, 48, 2, 'HTML CLASSES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(63, 15, 49, 50, 2, 'HTML IFRAMES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(64, 15, 51, 52, 2, 'HTML ID', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(65, 15, 53, 54, 2, 'HTML JAVASCRIPT', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(66, 15, 55, 56, 2, 'HTML HEAD', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(67, 15, 57, 58, 2, 'HTML FOOTER', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(68, 15, 59, 60, 2, 'HTML RESPONSIVE', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(69, 15, 61, 62, 2, 'HTML ENTITIES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(70, 15, 63, 64, 2, 'HTML SYMBOLS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(71, 15, 65, 66, 2, 'HTML URL ENCODE', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(74, 74, 1, 14, 0, 'CSS3', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(75, 74, 2, 3, 1, 'CSS3 PROPERTIES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(76, 74, 4, 5, 1, 'CSS3 NESTED CLASSES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(77, 74, 6, 7, 1, 'CSS3 FONTS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(78, 74, 8, 9, 1, 'CSS LESS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(79, 74, 10, 11, 1, 'CSS SAAS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(80, 74, 12, 13, 1, 'CSS3 RULES', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(81, 81, 1, 10, 0, 'JAVASCRIPT FRAMEWORK', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(82, 81, 2, 3, 1, 'ANGLUAR', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(83, 81, 4, 5, 1, 'NODE JS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(84, 81, 6, 7, 1, 'VUE JS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(85, 81, 8, 9, 1, 'REACT JS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(86, 86, 1, 10, 0, 'PHP FRAMEWORKS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(87, 86, 2, 3, 1, 'YII FRAMEWORK', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(88, 86, 4, 5, 1, 'LARAVEL', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(89, 86, 6, 7, 1, 'CODEIGNITER', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(90, 86, 8, 9, 1, 'SYMPHONY', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(91, 91, 1, 10, 0, 'BOOTSTRAP', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(92, 91, 2, 3, 1, 'MATERIAL DESIGN', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(93, 91, 4, 5, 1, 'GRID SYSTEM', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(94, 91, 6, 7, 1, 'BOOTSTRAP PLUGINS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1),
	(95, 91, 8, 9, 1, 'BOOTSTRAP ELEMENTS', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1);
/*!40000 ALTER TABLE `left_tree` ENABLE KEYS */;

-- Dumping structure for table yii2advanced.left_tree_item_description
DROP TABLE IF EXISTS `left_tree_item_description`;
CREATE TABLE IF NOT EXISTS `left_tree_item_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `left_tree_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_left_tree_item_description_left_tree` (`left_tree_id`),
  CONSTRAINT `FK_left_tree_item_description_left_tree` FOREIGN KEY (`left_tree_id`) REFERENCES `left_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table yii2advanced.left_tree_item_description: ~5 rows (approximately)
DELETE FROM `left_tree_item_description`;
/*!40000 ALTER TABLE `left_tree_item_description` DISABLE KEYS */;
INSERT INTO `left_tree_item_description` (`id`, `content`, `left_tree_id`) VALUES
	(1, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p><p> consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 12),
	(2, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt </p><p>mollit anim id est laborum.</p>', 13),
	(3, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing </p><p>elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 14),
	(4, '<hr><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, </p><p>sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 15),
	(5, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p><br></p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 7),
	(6, '<ol><li>array_filter()</li><li>array_merge()</li><li>array_flip()</li><li>array_push()</li><li>array_combine()</li><li>array_search()</li><li>is_array()</li><li>array_walk()</li><li>array_walk_recursive()</li></ol>', 6);
/*!40000 ALTER TABLE `left_tree_item_description` ENABLE KEYS */;

-- Dumping structure for table yii2advanced.migration
DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table yii2advanced.migration: ~2 rows (approximately)
DELETE FROM `migration`;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1534414432),
	('m130524_201442_init', 1534414442);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;

-- Dumping structure for table yii2advanced.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table yii2advanced.user: ~1 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'webmaster', 'rl03zmHsMZQZGwXYqXebn9JHgXp26lda', '$2y$13$gsRQn7SoRDlxbHlrw6zT8.MQCLPfGfF/JUto2NgTXEtj9E/5ebPMK', NULL, 'pardeeparora69@gmail.com', 10, 1534414597, 1534414597);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
